import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:test_unsplash/detail.dart';
import 'package:test_unsplash/functions.dart';
import 'package:test_unsplash/model.dart';

List<LoadedImage> imageList = new List();

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Future fetchData(String barcode) async {
    try {
      final response = await http.get(
        'https://api.unsplash.com/photos/?client_id=ab3411e4ac868c2646c0ed488dfd919ef612b04c264f3374c97fff98ed253dc9',
      );
      print(response.statusCode);
      if (response.statusCode == 200) {
        var jsonData = jsonDecode(response.body);
        for (var u in jsonData) {
          //print(u['user']);
          LoadedImage temp = LoadedImage(
              u['urls']['thumb'], u['urls']['full'], u['user']['name']);
          imageList.add(temp);
          // print(temp.large);
        }
      } else {
        messageDialog(context,
            'Не удалось загрузить фотографии. Пожалуйста, попробуйте позднее!');
        throw Exception('Failed to load album');
      }
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue.shade500,
        title: Text('Галерея'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
            //direction: Axis.vertical,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(0, 0, 00, 20),
                height: MediaQuery.of(context).size.height,
                child: FutureBuilder(
                    future: fetchData('тест'),
                    builder: (context, snapshot) {
                      return new ListView.builder(
                        itemCount: imageList.length,
                        itemBuilder: (BuildContext context, int index) {
                          return ListTile(
                            //tileColor: index%2==0? Colors.black12:Colors.white,
                            leading: Text((index + 1).toString()),
                            title: Text(imageList[index].username),
                            trailing: Container(
                              color: Colors.black,
                              width: 50,
                              height: 50,
                              child: ClipRRect(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(5),
                                      topRight: Radius.circular(5)),
                                  child: AspectRatio(
                                      aspectRatio: 6 / 3.5,
                                      child: Image.network(
                                        imageList[index].thumb,
                                        loadingBuilder: (BuildContext context,
                                            Widget child,
                                            ImageChunkEvent loadingProgress) {
                                          if (loadingProgress == null)
                                            return child;
                                          return Center(
                                            child: CircularProgressIndicator(
                                              value: loadingProgress
                                                          .expectedTotalBytes !=
                                                      null
                                                  ? loadingProgress
                                                          .cumulativeBytesLoaded /
                                                      loadingProgress
                                                          .expectedTotalBytes
                                                  : null,
                                            ),
                                          );
                                        },
                                      ))),
                            ),
                            onTap: () async {
                              // fetchData(lUser.invoices[index].number).whenComplete(() =>
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          DetailPage(imageList[index])));
                              //  );
                            },
                          );
                        },
                      );
                    }),
              ),
            ]),
      ),
    );
  }
}
