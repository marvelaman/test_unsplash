import 'package:flutter/material.dart';
import 'package:test_unsplash/home.dart';

void main() {
  runApp(App());
}

String passwordSav;

class App extends StatelessWidget {
  App({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        'HomePage': (context) => HomePage(),
      },
      home: HomePage(),
    );
  }
}
