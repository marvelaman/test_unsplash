class LoadedImage {
  final String username;
  final String thumb;
  final String large;

  LoadedImage(this.thumb, this.large, this.username);
}
