import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void messageDialog(context, String message) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: Colors.transparent,
          child: Container(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            height: 150,
            width: 200,
            decoration: BoxDecoration(
                color: Colors.white70.withOpacity(0.9),
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(
                    color: Colors.black38.withOpacity(0.24),
                    width: 1.5,
                    style: BorderStyle.solid)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  message,
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        );
      });
}

void actionDialog(context, String message, Function func) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: Colors.transparent,
          child: Container(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            height: 150,
            width: 200,
            decoration: BoxDecoration(
                color: Colors.white70.withOpacity(0.9),
                borderRadius: BorderRadius.all(Radius.circular(10)),
                border: Border.all(
                    color: Colors.black38.withOpacity(0.24),
                    width: 1.5,
                    style: BorderStyle.solid)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  message,
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    FlatButton(
                      color: Colors.green,
                      onPressed: () {
                        Navigator.pop(context);
                        func(message);
                      },
                      child: Text('Подтвердить'),
                    ),
                    SizedBox(width: 16),
                    FlatButton(
                      color: Colors.redAccent.withOpacity(0.8),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text('Отменить'),
                    )
                  ],
                ),
              ],
            ),
          ),
        );
      });
}
